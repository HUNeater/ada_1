with Ada.Text_IO, tablesort, table, selects;
use Ada.Text_IO;

procedure main is	
function People_Select_Test return Boolean is
			procedure Selects_1 is new Selects(OldItem => People, OldTable_Array => People_Array, NewItem => NewPeople_1, NewTable_Array => New_People_Array_1, OldTable_Type => People_Table, NewTable_Type => New_People_Table_1, Transfer => transfer1);
			procedure Selects_2 is new Selects(OldItem => People, OldTable_Array => People_Array, NewItem => NewPeople_2, NewTable_Array => New_People_Array_2, OldTable_Type => People_Table, NewTable_Type => New_People_Table_2, Transfer => transfer2);
			People_Select_3 : People_Table.Table_Type(3);
			People_Select_3_New_1 : New_People_Table_1.Table_Type(3);
			People_Select_3_New_2 : New_People_Table_2.Table_Type(3);
			People_Temp_1 : New_People_Array_1(1..3);
			People_Temp_2 : New_People_Array_2(1..3);
		begin
			for I in Positive range 1..3 loop
                begin
                    People_Table.Insert(People_Select_3,(Kor=>I*3+5,Money=>I*200+10000));
                end;
            end loop;
			Selects_1(People_Select_3, People_Select_3_New_1);
			Selects_2(People_Select_3, People_Select_3_New_2);
			People_Temp_1 := New_People_Table_1.GetTable(People_Select_3_New_1);
			People_Temp_2 := New_People_Table_2.GetTable(People_Select_3_New_2);
			for J in Positive range 1..3 loop
				if People_Temp_1(J).Kor /= J*3+5 then
					return false;
				end if;
			end loop;
			for J in Positive range 1..3 loop
				if People_Temp_2(J).Money /= J*200+10000 then
					return false;
				end if;
			end loop;
			return true;
		end People_Select_Test;

begin
	null;	
end main;
