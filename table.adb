with tablesort;

package body table is
	function Size(tt : Table_Type) return Positive is begin
		return tt.count; 
	end Size;

	function getTable(tt : Table_Type) return Table_Array is begin
		return tt.table;
	end getTable;


	procedure Insert(tt : in out Table_Type; i : Item) is begin
		if tt.count < tt.cpct then
			tt.count := tt.count + 1;
			tt.table(tt.count) := i;
		end if;
	end Insert;


	procedure tableWhere(tt : Table_Type; ta : out Table_Array; n : out Natural) is begin
		n := 0;

		for i in tt.table'range loop
			if op(tt.table(i)) then
				n := n + 1;
				ta(n) := tt.table(i);
			end if;
		end loop;
	end tableWhere;


	procedure table_Sort(tt : in out Table_Type) is
		procedure sort is new tablesort(Item, Positive, Table_Array, "<");       
	begin
		sort(tt.table);
	end table_Sort;
end table;

