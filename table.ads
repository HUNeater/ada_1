generic
	type Item is private;
	type Table_Array is array(Positive range <>) of Item;
package table is
	type Table_Type(Capacity : Positive) is private;

	function Size(tt: Table_Type) return Positive;
	function getTable(tt : Table_Type) return Table_Array;
	procedure Insert(tt : in out Table_Type; i : Item);

	generic
		with function op(i : Item) return Boolean;
	procedure tableWhere(tt : Table_Type; ta : out Table_Array; n : out Natural);

	generic
		with function "<"(lhs, rhs : Item) return Boolean;
	procedure table_Sort(tt : in out Table_Type);

	private
		type Table_Type(Capacity : Positive) is record
			count : Natural := 0;
			cpct : Positive := Capacity;
			table : Table_Array(1 .. Capacity);
		end record;
end table;	

