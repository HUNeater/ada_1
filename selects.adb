with table;

procedure Selects(int : OldTable_Type.Table_Type; outt : out NewTable_Type.Table_Type) is 
	int_t : OldTable_Array := OldTable_Type.getTable(int);
	out_t : NewTable_Array := NewTable_Type.getTable(outt);
begin
	for i in int_t'range loop
		out_t(i) := Transfer(int_t(i));
	end loop;
end Selects;
