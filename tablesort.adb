procedure tableSort(v : in out TableArray) is
i : Index;
w : Item;

pragma Suppress(Range_Check);
begin
	for j in v'first .. Index'pred(v'last) loop
		i := j;	
		w := v(Index'succ(j));
		
		while i >= v'first and then "<"(w, v(i)) loop
			v(Index'succ(i)) := v(i);
			i := Index'pred(i);
		end loop;

		v(Index'succ(i)) := w;
	end loop;	
end tableSort;
