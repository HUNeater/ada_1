generic
	type Item is private;
	type Index is (<>);
	type TableArray is array(Index range <>) of Item;
	with function "<"(lhs,rhs : Item) return Boolean;

procedure tableSort(v : in out TableArray);
