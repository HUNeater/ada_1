with Ada.Text_IO, TableSort;
use Ada.Text_IO;

procedure mainsort is

    function TestSort return Boolean is
        -- Definialjon a sabloneljaras ket peldanyat! Az alabbi modon:
            -- Legyen ket függveny ami csökkenö es növekvö sorrendbe rendet;
            -- Definialjon egy Positiv ertekekkel indexelö Integer ertekeket taroló tömböt;
            -- Hozzon letre a sablonfüggvenynek ket peldanyat ezekböl;
            -- Hozzon letre tesztelö tömböket;

        -- Ezt a függvenyt ne bantsa!

	function csokkeno(lhs,rhs : Integer) return Boolean is
	begin
		return lhs > rhs;
	end csokkeno;

	function novekvo(lhs, rhs : Integer) return Boolean is
	begin
		return lhs < rhs;
	end novekvo;

	type Int_Array is array(Positive range <>) of Integer;
	procedure fv1 is new tablesort(Integer, Positive, Int_Array, csokkeno);
	procedure fv2 is new tablesort(Integer, Positive, Int_Array, novekvo);

	v1 : Int_Array := (1, 2, 3, 4, 5);
	v11 : Int_Array := (5, 4, 3, 2, 1);
	
	v2 : Int_Array := (1, 2, 3, 4, 5);
	v22 : Int_Array := (1, 2, 3, 4, 5);

	v3 : Int_Array := (1, 3, 2, 5, 4);
	v33 : Int_Array := (5, 4, 3, 2, 1);

	v4 : Int_Array := (1, 3, 2, 5, 4);
	v44 : Int_Array := (1, 2, 3, 4, 5);
        
	function Correct(
            SortT : access procedure (A: in out Int_Array);
            Input: in out Int_Array;
            Output: in Int_Array
        ) return Boolean
        is
        begin
            SortT(Input);
            for I in Input'Range loop
                if Input(I) /= Output(I) then
                    return false;
                end if;
            end loop;
            return true;
        exception
            when others => return false;
        end Correct;

        -- Idaig tartott a Correct függveny!


    begin
        -- A Testsort függveny terjen vissza a Correct függveny segitsegevel egy 
        -- Igaz vagy hamis allitassal.
        -- Correct függveny hasznalata:
            -- elsö parameter: A sabloneljaras egy peldanyanak a neve. 
                -- Peldaul : Sort_Asc (sabloneljaras) akkor az elsö parameter helyere irja azt, hogy
                -- Sort_Asc'Access
            -- masodik parameter: A rendezni kivant tömb
            -- harmadik parameter: A helyesen rendezett tömb
  
    	return Correct(fv1'access, v1, v11) and
	       Correct(fv2'access, v2, v22) and
	       Correct(fv1'access, v3, v33) and
	       Correct(fv2'access, v4, v44);

    end TestSort;

begin
    if not TestSort then
        Put_Line("Megbukott a teszteken :-( ");
    else
        Put_Line("atment a teszteken :-) ");   
    end if;
end mainsort;
