with table;

generic
	type OldItem is private;
	type OldTable_Array is array(Positive range <>) of OldItem;
	type NewItem is private;
	type NewTable_Array is array(Positive range <>) of NewItem;
	with package OldTable_Type is new Table(OldItem, OldTable_Array);
	with package NewTable_Type is new Table(NewItem, NewTable_Array);
	with function Transfer(current : OldItem) return NewItem;
procedure Selects(int : OldTable_Type.Table_Type; outt : out NewTable_Type.Table_Type);
